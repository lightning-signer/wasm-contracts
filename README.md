# Description

A system for programmable Bitcoin off-chain contracts.  Features:

* contract code is written in a functional subset of Rust
* contract code execution is sandboxed in a WebAssembly container

Here is an example of a [swap contract](contracts/src/contracts/swap.rs) in oracle mode.

And a [call option contract](contracts/src/contracts/option.rs) in arbitrator mode.

The sandboxing mechanism allows dynamic addition of new contract types.

This is meant to work with [Discreet Log Contracts](https://github.com/discreetlogcontracts/dlcspecs/).

# Use Cases

Two usecase types are envisioned, Oracle Mode and Arbitrator Mode.

## Oracle Mode

In this mode, an oracle advertises an event, and after the event occurs it broadcasts an outcome, which is a set of signatures.
The parties to an Oracle DLC contract create appropriate contract execution transactions (CETs) and some subset of them
become valid when the outcome is broadcast.  The logic of the contract must be completely expressed via
the CETs, which implies a limited level of expression. 

An example would be a swap contract, where there is a transfer function from the price of an asset to the
BTC outcome for the parties.

## Arbitrator Mode

In this mode, a semi-trusted third party arbitrates any disagreement regarding the outcome.
Arbitrary contract logic can be executed in the arbitrator on a per-contract basis, which implies a much
more general level of expression.
The sandboxing mechanism is more interesting in this case, because it can safely execute
more complex logic that doesn't have to be pre-installed on the arbitrator.

An example would be an option contract, where Alice can decide to exercise at any time, so the specific
external event is not known ahead of time because it depends on Alice's choice and its timing.

# Status

Proof of concept.

Requires Rust 1.40 due to cranelift dependencies for the container.  The contracts can be run in Rust 1.39
(or possibly a bit earlier).

# Roadmap

* Deterministic contract to `*.wasm` compilation
* Deterministic toolchain build (probably via Guix)
* Communication protocol
* m-of-n oracles / arbitrators

Oracle mode:
* Oracle setup publishing - publish possible outcomes and their public key
* Oracle outcome publishing - publish the actual outcome and the its signature
* Create contract from published Oracle setup

Arbitrator mode:
* A driver for running arbitrator contract logic

# Setup

See `scripts/setup.sh` for the required Rust toolchain component to target `wasm32`.

# Testing

See `scripts/test.sh`.
