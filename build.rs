use std::process::{Command, Stdio};
use walkdir::WalkDir;

fn main() {
    let mut cmd =
        Command::new("cargo");
    cmd.args(&["-v", "build", "--target=wasm32-unknown-unknown"]);
    cmd.current_dir("contracts");
    cmd.stdout(Stdio::inherit())
        .stderr(Stdio::inherit());
    let success = cmd.output().expect("failed").status.success();
    if !success {
        panic!("failed")
    }
    for entry in WalkDir::new("contracts") {
        let dir_entry = entry.expect("non-unicode path");
        let path_str = dir_entry.path().to_str().unwrap();
        if path_str.ends_with(".rs") || path_str.ends_with("/Cargo.toml") || path_str.ends_with("Cargo.lock") {
            println!("cargo:rerun-if-changed={}", path_str);
        }
    }
}