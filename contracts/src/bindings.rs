use serde_cbor::ser::Write;
use serde_cbor::de::MutSliceRead;
use crate::invoke::{SimpleResult, Invocation};
use serde::{Deserialize, Serialize};

pub const MAX_XFER_SIZE: usize = 1024;

static mut OUT_BUFFER: [u8; MAX_XFER_SIZE] = [0u8; MAX_XFER_SIZE];

#[no_mangle]
pub extern "C" fn in_buffer_size() -> i64 {
    MAX_XFER_SIZE as i64
}

#[no_mangle]
pub extern "C" fn out_buffer_address() -> i64 {
    unsafe {
        let addr = OUT_BUFFER.as_ptr();
        return addr as i64;
    }
}

static mut IN_BUFFER: [u8; MAX_XFER_SIZE] = [0u8; MAX_XFER_SIZE];

#[no_mangle]
pub extern "C" fn in_buffer_address() -> i64 {
    unsafe {
        let addr = IN_BUFFER.as_ptr();
        return addr as i64;
    }
}

pub struct MemoryWriter<'a> {
    cursor: &'a mut usize,
}

impl Write for MemoryWriter<'_> {
    type Error = serde_cbor::Error;

    fn write_all(&mut self, buf: &[u8]) -> Result<(), serde_cbor::Error> {
        for octet in buf {
            unsafe {
                OUT_BUFFER[*self.cursor] = *octet;
            }
            *self.cursor += 1;
        }
        Ok(())
    }
}

pub fn new_writer(cursor: &mut usize) -> MemoryWriter {
    MemoryWriter { cursor }
}

pub fn new_reader<'a>() -> MutSliceRead<'a> {
    unsafe {
        MutSliceRead::new(&mut IN_BUFFER)
    }
}

pub fn deserialize_invocation<Instance: Deserialize<'static>>() -> Invocation<Instance> {
    let reader = new_reader();
    deserialize_invocation_from_reader(reader)
}

pub(crate) fn deserialize_invocation_from_reader<'a, Instance: Deserialize<'a>>(reader: MutSliceRead<'a>) -> Invocation<Instance> {
    let mut deser = serde_cbor::Deserializer::new(reader);
    let result = Deserialize::deserialize(&mut deser);
    if result.is_err() {
        let s = &result.err().unwrap().to_string();
        write_error(s);
    }
    let invocation: Invocation<Instance> = result.unwrap();
    // a data point for the start and end of the period, for each ID
    assert_eq!(invocation.contract.oracle_data_ids.len() * 2, invocation.oracle_data.len());
    invocation
}

// FIXME
fn write_error(err: &String) -> ! {
    let mut cursor: usize = 0;
    let mut w = new_writer(&mut cursor);
    w.write_all(err.as_bytes()).unwrap();
    w.write_all(&[0]).unwrap();
    panic!();
}

pub fn serialize_simple_result(result: SimpleResult) -> i64 {
    let mut cursor: usize = 0;
    let w = new_writer(&mut cursor);
    let mut serializer = serde_cbor::Serializer::new(w);
    result.serialize(&mut serializer).unwrap();
    cursor as i64
}