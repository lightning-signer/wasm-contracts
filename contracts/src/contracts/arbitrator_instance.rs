use super::event::PriceEvent;

pub struct ArbitratedOutcome {
    /// Result for first party
    pub quantity: i64,
}

pub enum OutcomeError {
    /// We cannot determine the outcome yet
    NotYet {
        /// Hint that you can retry at this time, or when next action comes in
        retry_at: u64
    },
    /// We refuse to determine an outcome, because wrong actions were supplied
    InvalidAction(String),
    /// Numeric overflow
    Overflow,
}

pub trait ArbitratedContractInstance {
    fn outcome(&self, event: PriceEvent, now: u64) -> Result<ArbitratedOutcome, OutcomeError>;
}