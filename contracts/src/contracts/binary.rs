use serde::{Deserialize, Serialize};
use super::oracle_instance::OracleContractInstance;
use super::event::PriceEvent;

/// A binary option contract, where the first party is betting the end price being strictly
/// greater than the target price.
/// The period start price is ignored.
#[derive(Serialize, Deserialize)]
pub struct BinaryContractInstance {
    target_price: u64,
}

impl BinaryContractInstance {
    pub fn new(target_price: u64) -> Self {
        BinaryContractInstance { target_price }
    }
}

impl OracleContractInstance for BinaryContractInstance {
    /// Outcome number to be settled - 0 if under target price, 1 if over.
    fn outcome(&self, event: PriceEvent) -> Result<u32, ()> {
        if event.end_price > self.target_price {
            Ok(1)
        } else {
            Ok(0)
        }
    }

    fn quantity_for_outcome(&self, quantity: u64, outcome: u32) -> Result<i64, ()> {
        if outcome == 0 {
            Ok(-(quantity as i64))
        } else {
            Ok(quantity as i64)
        }
    }

    fn num_outcomes(&self) -> u32 {
        2
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn make_contract_instance() -> BinaryContractInstance {
        BinaryContractInstance {
            target_price: 10000
        }
    }

    #[test]
    fn test_outcome() -> Result<(), ()> {
        let instance = make_contract_instance();

        assert_eq!(instance.outcome(PriceEvent::new(0, 10001))?, 1);
        assert_eq!(instance.outcome(PriceEvent::new(0, 9999))?, 0);
        assert_eq!(instance.outcome(PriceEvent::new(0, 10000))?, 0);

        let outcomes: Vec<i64> = instance.quantity_outcome_iter(1000).collect();
        assert_eq!(outcomes, vec![-1000, 1000]);
        Ok(())
    }
}