use crate::bindings::{deserialize_invocation, serialize_simple_result};
use crate::invoke::{Invocation, SimpleResult};
use super::binary::BinaryContractInstance;
use super::oracle_instance::OracleContractInstance;
use super::event::PriceEvent;

#[no_mangle]
pub extern "C" fn binary_driver() -> i64 {
    let invocation: Invocation<BinaryContractInstance> = deserialize_invocation();
    let oracle_data = invocation.oracle_data;
    let event = PriceEvent::new(oracle_data[0], oracle_data[1]);
    let outcome = invocation.contract.instance.outcome(event)
        .expect("contract execution error");
    let result = SimpleResult { outcome };
    serialize_simple_result(result)
}
