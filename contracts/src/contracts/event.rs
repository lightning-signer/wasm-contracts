/// An event for a price change in a time interval, with a start and end price.
#[derive(Clone, Copy)]
pub struct PriceEvent {
    /// Price at the start of the period
    pub start_price: u64,
    /// Price at the end of the period
    pub end_price: u64,
}

impl PriceEvent {
    pub fn new(start_price: u64, end_price: u64) -> Self {
        PriceEvent {
            start_price,
            end_price
        }
    }

    pub fn delta(&self) -> i64 {
        self.end_price as i64 - self.start_price as i64
    }
}

