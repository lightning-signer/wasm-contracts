use super::event::PriceEvent;
use super::arbitrator_instance::OutcomeError::{NotYet, Overflow, InvalidAction};
use super::arbitrator_instance::{ArbitratedOutcome, OutcomeError, ArbitratedContractInstance};
use OptionAction::Exercise;

pub struct CallOptionContractInstance {
    // Expiration in millis since the epoch
    pub expiration: u64,
    // Strike price
    pub strike_price: u64,
    /// The maximum change in the price in rational form.
    /// Any change at and beyond the maximum results in one party getting the entire amount
    /// of the bet.
    /// example: (3, 100) would be 3% max change in price
    pub max_change: (i64, u64),
    /// Collateral for party 0 (only to punish cheating)
    pub collateral_party_0: u64,
    /// Collateral for party 1
    pub collateral_party_1: u64,
    /// Exercise action
    pub action: Option<OptionAction>,
}

pub enum OptionAction {
    Exercise {
        at: u64,
        party: u8,
    }
}

impl CallOptionContractInstance {
    pub fn with_exercise_action(self, action: OptionAction) -> Result<Self, OutcomeError> {
        if self.action.is_some() {
            return Err(InvalidAction("already exercised".to_string()))
        }
        let Exercise { at, party } = action;
        if self.expiration < at {
            return Err(InvalidAction("already expired".to_string()))
        }
        if party != 0 {
            return Err(InvalidAction("party cannot take action".to_string()))
        }
        Ok(CallOptionContractInstance {
            action: Some(action),
            ..self
        })
    }
}

impl ArbitratedContractInstance for CallOptionContractInstance {
    /// The event is the most recent price before "now"
    fn outcome(&self, event: PriceEvent, now: u64) -> Result<ArbitratedOutcome, OutcomeError> {
        if self.action.is_none() && now < self.expiration {
            // If no exercise, we can't have an outcome until expiration
            return Err(NotYet { retry_at: self.expiration })
        }

        // At this point, either no action was taken and we expired, or we have a valid exercise
        if event.end_price < self.strike_price {
            // Expired or exercised worthless, return party 0's security deposit
            return Ok(ArbitratedOutcome { quantity: 0 })
        }

        let price_delta = event.end_price - self.strike_price;

        let total_value = self.collateral_party_0 + self.collateral_party_1;

        // At max change above strike, party 1 loses entire collateral
        let quantity =
                total_value.checked_mul(price_delta).ok_or(Overflow)?
                    .checked_mul(self.max_change.1).ok_or(Overflow)? as i64
                    / self.max_change.0 / (self.strike_price as i64).max(self.collateral_party_1 as i64);
        Ok(ArbitratedOutcome { quantity })
    }
}
