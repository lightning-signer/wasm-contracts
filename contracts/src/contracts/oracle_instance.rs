use super::oracle_iter::OracleIter;
use super::event::PriceEvent;

/// A contract instantiation
pub trait OracleContractInstance {
    /// Outcome number to be settled - in the range [0 to self.num_outcomes).
    fn outcome(&self, event: PriceEvent) -> Result<u32, ()>;

    /// Quantity to be received by first party from the funding collateral for a specific outcome number.
    /// Will be negative if the market moved against the first party.
    /// The quantity argument is the quantity each party has put into the contract.
    fn quantity_for_outcome(&self, quantity: u64, outcome: u32) -> Result<i64, ()>;

    /// Quantity to be received by first party from the funding collateral.
    /// Will be negative if the market moved against the first party.
    /// The quantity argument is the quantity each party has put into the contract.
    fn quantity_for_event(&self, event: PriceEvent, quantity: u64) -> Result<i64, ()> {
        let outcome = self.outcome(event)?;
        self.quantity_for_outcome(quantity, outcome)
    }

    /// Total number of outcomes
    fn num_outcomes(&self) -> u32;

    /// An iterator over the possible outcomes, returning the quantity outcome for the first party.
    /// The outcomes are in order starting at outcome number zero.
    /// The quantity argument is the quantity each party has put into the contract.
    fn quantity_outcome_iter(&self, quantity: u64) -> OracleIter<Self>
        where Self: Sized {
        OracleIter::new(quantity, &self)
    }
}
