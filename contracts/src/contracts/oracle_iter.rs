use super::oracle_instance::OracleContractInstance;

/// An iterator over the possible outcomes, returning the quantity outcome for the first party.
/// The outcomes are in order starting at outcome number zero.
pub struct OracleIter<'a, CD: OracleContractInstance> {
    outcome: u32,
    quantity: u64,
    instance: &'a CD,
}

impl<CD: OracleContractInstance> OracleIter<'_, CD> {
    /// Create the iterator.
    /// The quantity argument is the quantity each party has put into the contract.
    pub fn new(quantity: u64, instance: &CD) -> OracleIter<'_, CD> {
        OracleIter {
            outcome: 0,
            quantity,
            instance,
        }
    }
}

impl<CD: OracleContractInstance> Iterator for OracleIter<'_, CD> {
    type Item = i64;

    fn next(&mut self) -> Option<Self::Item> {
        if self.outcome < self.instance.num_outcomes() {
            let quantity_for_a = self.instance.quantity_for_outcome(self.quantity, self.outcome).unwrap();
            self.outcome += 1;
            Some(quantity_for_a)
        } else {
            None
        }
    }
}

