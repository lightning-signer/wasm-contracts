use serde::{Deserialize, Serialize};

use super::oracle_instance::OracleContractInstance;
use super::event::PriceEvent;

/// An oracle-mode swap contract, where Alice is betting on an increase in price of the underlying asset.
/// The settlement is in the underlying asset.
#[derive(Serialize, Deserialize, PartialEq)]
pub struct SwapContractInstance {
    /// The maximum change in the price in rational form.
    /// Any change at and beyond the maximum results in one party getting the entire amount
    /// of the bet.
    /// example: (3, 100) would be 3% max change in price
    max_change: (i64, u64),
    /// We handle num_outcome different outcomes spread uniformly from -max to +max
    /// where the positive end is associated with a high price.
    /// Outcome zero results in Bob getting the entire bet, while
    /// outcome = num_outcomes - 1 results in Alice getting the entire bet.
    num_outcomes: u32,
}

impl SwapContractInstance {
    pub fn new(max_change: (i64, u64), num_outcomes: u32) -> Result<Self, ()> {
        let data = SwapContractInstance { max_change, num_outcomes };
        data.validate()
    }

    pub fn validate(self) -> Result<Self, ()> {
        if self.num_outcomes < 2 {
            return Err(())
        }
        if self.max_change.0 <= 0 {
            return Err(())
        }
        if self.max_change.1 == 0 {
            return Err(())
        }
        Ok(self)
    }
}

impl OracleContractInstance for SwapContractInstance {
    fn outcome(&self, event: PriceEvent) -> Result<u32, ()> {
        if event.start_price == 0 {
            return Err(())
        }

        // the actual range is [-num_outcomes / 2, +num_outcomes / 2] - compute the maximum
        // in one direction
        let max_outcome = self.num_outcomes as i64 / 2;
        // compute the actual signed outcome
        let outcome = (
            event.delta().checked_mul(self.max_change.1 as i64).ok_or(())?
                .checked_mul(max_outcome).ok_or(())?
        ) / self.max_change.0 / event.start_price as i64;

        // compute the actual unsigned outcome
        let unsigned_outcome = outcome + max_outcome;

        // clamp the outcome in case the price changed by more than the max
        let clamped_outcome =
            unsigned_outcome.max(0).min(self.num_outcomes as i64 - 1);

        Ok(clamped_outcome as u32)
    }

    // the actual quantity for Alice given the bet size for each side and the actual unsigned outcome
    fn quantity_for_outcome(&self, quantity: u64, outcome: u32) -> Result<i64, ()> {
        let adjusted_outcome = outcome as i64 * 2 - self.num_outcomes as i64 + 1;
        let outcome_quantity = (((quantity as i64)
            .checked_mul(self.max_change.0).ok_or(())?
            .checked_mul(adjusted_outcome).ok_or(())?)
            / self.max_change.1 as i64)
            / (self.num_outcomes as i64 - 1);
        Ok(outcome_quantity)
    }

    fn num_outcomes(&self) -> u32 {
        self.num_outcomes
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn with_end_price(end_price: u64) -> PriceEvent { PriceEvent::new(100, end_price)}

    #[test]
    fn test_overflow() {
        let data = SwapContractInstance::new((3, 100), u32::max_value()).unwrap();
        let outcome = data.outcome(PriceEvent::new(10000000000, 11000000000));
        outcome.expect_err("expected overflow");
    }

    fn make_contract_instance() -> SwapContractInstance {
        SwapContractInstance::new((3, 100), 255).unwrap()
    }

    #[test]
    fn test_outcome_basic_odd() -> Result<(), ()>{
        let instance = SwapContractInstance::new((10, 100), 3).unwrap();
        assert_eq!(instance.outcome(with_end_price(0))?, 0);
        assert_eq!(instance.outcome(with_end_price(90))?, 0);
        assert_eq!(instance.outcome(with_end_price(91))?, 1);
        assert_eq!(instance.outcome(with_end_price(109))?, 1);
        assert_eq!(instance.outcome(with_end_price(110))?, 2);
        assert_eq!(instance.outcome(with_end_price(200))?, 2);
        let outcomes: Vec<i64> = instance.quantity_outcome_iter(1000).collect();
        assert_eq!(outcomes, vec![-100, 0, 100]);
        Ok(())
    }

    #[test]
    fn test_iter() {
        let instance4 = SwapContractInstance::new((10, 100), 4).unwrap();
        let outcomes4: Vec<i64> = instance4.quantity_outcome_iter(1000).collect();
        assert_eq!(outcomes4, vec![-100, -33, 33, 100]);

        let instance5 = SwapContractInstance::new((10, 100), 5).unwrap();
        let outcomes5: Vec<i64> = instance5.quantity_outcome_iter(1000).collect();
        assert_eq!(outcomes5, vec![-100, -50, 0, 50, 100]);
    }

    #[test]
    fn test_outcome_basic_even() -> Result<(), ()>{
        let instance = SwapContractInstance::new((10, 100), 2).unwrap();
        assert_eq!(instance.outcome(with_end_price(0))?, 0);
        assert_eq!(instance.outcome(with_end_price(90))?, 0);
        assert_eq!(instance.outcome(with_end_price(100))?, 1);
        assert_eq!(instance.outcome(with_end_price(200))?, 1);
        let outcomes: Vec<i64> = instance.quantity_outcome_iter(1000).collect();
        assert_eq!(outcomes, vec![-100, 100]);
        Ok(())
    }

    #[test]
    fn test_outcome() -> Result<(), ()>{
        let instance = make_contract_instance();

        // increase 1%, which is 1/3 of the max range
        let end_price_up = 10100;
        let event_up = PriceEvent::new(10000, end_price_up);
        assert_eq!(instance.outcome(event_up)?, 127 + 128/3);
        assert_eq!(instance.quantity_for_event(event_up, 1000)?, 9);
        assert_eq!(instance.quantity_for_event(event_up, 10000)?, 99);

        // null outcome
        let event_null = PriceEvent::new(10000, 10000);
        assert_eq!(instance.outcome(event_null)?, 127);
        assert_eq!(instance.quantity_for_event(event_null, 1000)?, 0);

        // decrease
        let end_price_down = 9900;
        let event_down = PriceEvent::new(10000, end_price_down);
        assert_eq!(instance.outcome(event_down)?, 127 - 128/3);
        assert_eq!(instance.quantity_for_event(event_down, 1000)?, -9);
        assert_eq!(instance.quantity_for_event(event_down, 10000)?, -99);
        Ok(())
    }

    #[test]
    fn test_outcome_clamped() -> Result<(), ()> {
        let instance = make_contract_instance();

        let end_price_up = 10500;
        let event_up = PriceEvent::new(10000, end_price_up);
        assert_eq!(instance.outcome(event_up)?, 254);
        assert_eq!(instance.quantity_for_event(event_up, 1000)?, 30);
        assert_eq!(instance.quantity_for_event(event_up, 10000)?, 300);

        // decrease
        let end_price_down = 9500;
        let event_down = PriceEvent::new(10000, end_price_down);
        assert_eq!(instance.outcome(event_down)?, 0);
        assert_eq!(instance.quantity_for_event(event_down, 1000)?, -30);
        assert_eq!(instance.quantity_for_event(event_down, 10000)?, -300);
        Ok(())
    }
}