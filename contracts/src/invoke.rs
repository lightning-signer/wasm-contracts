use serde::{Deserialize, Serialize};
#[cfg(any(std, test))]
use serde_cbor::value::Value;

#[derive(Serialize, Deserialize, PartialEq)]
pub struct Invocation<Instance> {
    /// current timestamp in milliseconds since the epoch
    pub now_ts: u64,
    /// the contract
    pub contract: Contract<Instance>,
    /// oracle data for each data ID at start and end of period
    pub oracle_data: Vec<u64>,
}

impl<ContractInstance> Invocation<ContractInstance> {
    pub fn new(now_ts: u64, contract: Contract<ContractInstance>, oracle_data: Vec<u64>) -> Self {
        if oracle_data.len() != contract.oracle_data_ids.len() * 2 {
            panic!("must have two data points for each oracle data ID");
        }
        Invocation {
            now_ts,
            contract,
            oracle_data,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq)]
pub struct Contract<ContractInstance> {
    /// start of period, millis since the epoch
    pub start_ts: u64,
    /// end of period, millis since the epoch
    pub end_ts: u64,
    /// the oracle data to supply to the contract evaluation
    pub oracle_data_ids: Vec<String>,
    /// the contract data
    pub instance: ContractInstance,
}

#[cfg(any(std, test))]
pub type GenericInvocation = Invocation<Value>;

/// A simple result with an outcome that is uniformly spread in the range 1 .. 2^n-1
#[derive(Serialize, Deserialize, PartialEq)]
pub struct SimpleResult {
    pub outcome: u32,
}
