extern crate alloc;
extern crate serde;
extern crate serde_cbor;

pub mod invoke;
pub mod contracts;
pub mod bindings;
pub mod util;

#[cfg(test)]
mod serde_tests;

// Use `wee_alloc` as the global allocator.
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

