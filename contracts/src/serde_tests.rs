#[cfg(test)]
mod tests {
    use crate::invoke::{GenericInvocation, Contract, Invocation};
    use crate::contracts::swap::SwapContractInstance;

    #[test]
    fn test_value() -> Result<(), ()> {
        let instance = SwapContractInstance::new((3, 100), 3)?;
        let contract = Contract {
            start_ts: 1000,
            end_ts: 1100,
            oracle_data_ids: vec!["price/BTC/kraken".to_string()],
            instance,
        };
        let orig = Invocation::new(123, contract, vec![10000, 11000]);
        let ser = serde_cbor::to_vec(&orig).unwrap();
        let deser_generic: GenericInvocation = serde_cbor::from_slice(ser.as_slice()).unwrap();
        let ser1 = serde_cbor::to_vec(&deser_generic).unwrap();
        assert_eq!(ser, ser1);
        let deser: Invocation<SwapContractInstance> = serde_cbor::from_slice(ser.as_slice()).unwrap();
        assert!(orig == deser);
        Ok(())
    }
}