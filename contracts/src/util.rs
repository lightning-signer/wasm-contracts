/// For integration testing
#[no_mangle]
pub extern "C" fn add(a: u32, b: u32) -> u32 {
    a + b
}

/// Should trigger a stack overflow, for integration testing
#[no_mangle]
pub extern "C" fn bad() -> u32 {
    inner(0)
}

fn inner(x: u32) -> u32 {
    if x < 100000 {
        1 | (inner(x + 1) << 1)
    } else {
        1
    }
}
