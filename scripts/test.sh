#!/bin/bash

set -e

cargo test
cargo run --bin integration-test
(cd contracts && cargo test)
