use wasmtime::{Engine, Config, Store, Linker, Module, Instance, InterruptHandle, Func, Memory, Trap};
use std::sync::Arc;
use contracts::invoke::{Invocation, SimpleResult};
use serde_cbor::ser::SliceWrite;
use serde_cbor::{Serializer, Value};
use serde::Serialize;
use anyhow::{Context, Error};
use contracts::contracts::oracle_instance::OracleContractInstance;
use contracts::bindings::MAX_XFER_SIZE;

const PATH: &str = "contracts/target/wasm32-unknown-unknown/debug/contracts.wasm";

pub struct Container {
    pub instance: Instance,
    pub interrupt_handle: Arc<InterruptHandle>,
    pub memory: Memory,
    pub out_buffer_address: usize,
    pub in_buffer_address: usize,
    pub in_buffer_size: usize
}

impl<'a> Container {
    pub fn new() -> anyhow::Result<Self> {
        let engine = Engine::new(
            Config::new()
                .interruptable(true)
        );
        let store = Store::new(&engine);
        let interrupt_handle = store.interrupt_handle()?;
        let mut linker = Linker::new(&store);

        let module = Module::from_file(store.engine(), PATH)?;
        linker.module("", &module)?;
        let instance = linker.instantiate(&module)?;

        let memory = instance
            .get_memory("memory")
            .expect("failed to find `memory` export");

        let out_buffer_address_f = Container::nullary_func(&instance, "out_buffer_address")?;

        let out_buffer_address = out_buffer_address_f()? as usize;

        let in_buffer_address_f = Container::nullary_func(&instance, "in_buffer_address")?;

        let in_buffer_address = in_buffer_address_f()? as usize;

        let in_buffer_size_f = Container::nullary_func(&instance, "in_buffer_size")?;

        let in_buffer_size = in_buffer_size_f()? as usize;

        Ok(Container {
            instance,
            interrupt_handle: Arc::new(interrupt_handle),
            memory,
            out_buffer_address,
            in_buffer_address,
            in_buffer_size,
        })
    }

    /// Start a thread that will interrupt the container, to protect against it taking too long
    pub fn start_interrupter(&self) {
        let handle = self.interrupt_handle.clone();
        std::thread::spawn(move || {
            std::thread::sleep(std::time::Duration::from_millis(1000));
            handle.interrupt();
        });
    }

    /// Call a contract, returning a simple result with just the outcome index
    pub fn typed_call_returning_simple_result<Instance: Serialize + OracleContractInstance>(
        &self,
        name: &str,
        invocation: Invocation<Instance>) -> anyhow::Result<SimpleResult> {
        self.serialize_invocation(invocation);

        let func = Container::nullary_func(&self.instance, name)?;

        let res = func();
        if res.is_err() {
            self.decode_error()?;
        }
        let size = res? as usize;
        assert!(size < MAX_XFER_SIZE);

        self.deserialize_result(size)
    }

    /// Call an untyped contract, returning a simple result with just the outcome index
    pub fn untyped_call_returning_simple_result(
        &self,
        name: &str,
        invocation: Invocation<Value>) -> anyhow::Result<SimpleResult> {
        self.serialize_invocation(invocation);

        let func = Container::nullary_func(&self.instance, name)?;

        let res = func();
        if res.is_err() {
            self.decode_error()?;
        }
        let size = res? as usize;
        assert!(size < MAX_XFER_SIZE);

        self.deserialize_result(size)
    }

    fn decode_error(&self) -> anyhow::Result<()> {
        unsafe {
            // FIXME
            let start_index = self.out_buffer_address;
            let end_index = self.out_buffer_address + MAX_XFER_SIZE;
            let slice = &self.memory.data_unchecked()[start_index..end_index];
            let ind = slice.iter().position(|x| *x == 0).unwrap();
            let s = &self.memory.data_unchecked()[start_index..start_index + ind];
            anyhow::bail!("failed {}", String::from_utf8(s.to_vec()).unwrap());
        }
    }

    fn deserialize_result(&self, size: usize) -> anyhow::Result<SimpleResult> {
        let result = unsafe {
            let start_index = self.out_buffer_address;
            let end_index = self.out_buffer_address + size;
            let slice = &self.memory.data_unchecked()[start_index..end_index];
            let result: SimpleResult = serde_cbor::from_slice(slice)
                .context("failed to deserialize")?;
            result
        };
        Ok(result)
    }

    fn serialize_invocation<ContractInstance: Serialize>(&self, invocation: Invocation<ContractInstance>) {
        unsafe {
            let start_index = self.in_buffer_address;
            let end_index = self.in_buffer_address + self.in_buffer_size;
            let input_slice = &mut self.memory.data_unchecked_mut()[start_index..end_index];
            let writer = SliceWrite::new(input_slice);
            let mut ser = Serializer::new(writer);
            invocation.serialize(&mut ser).unwrap();
        }
    }

    /// Find an arbitrary guest function, for integration testing
    pub fn get_func(&self, name: &str) -> Option<Func> {
        self.instance.get_func(name)
    }

    fn nullary_func(instance: &Instance, name: &str) -> anyhow::Result<impl Fn() -> Result<i64, Trap>> {
        instance.get_func(name)
            .ok_or_else(|| Error::msg(format!("failed to find `{}` function export", name)))?
            .get0::<i64>()
            .with_context(|| format!("failed to bind {}", name))
    }
}