extern crate serde;

use anyhow;
use contracts::contracts::swap::SwapContractInstance;
use contracts::invoke::{Contract, Invocation, SimpleResult};
use wasm_contracts::container::Container;

fn main() -> anyhow::Result<()> {
    run_all()
}

fn run_all() -> anyhow::Result<()> {
    let container = Container::new()?;

    container.start_interrupter();

    do_test_add(&container);

    do_test_stack_overflow(&container);

    do_swap_contract(&container)?;

    println!("success");
    Ok(())
}

fn do_test_add(container: &Container) {
    let add = container.get_func("add")
        .expect("failed to find `add` function export")
        .get2::<u32, u32, u32>().expect("mismatch");

    let res = add(3, 4).expect("failed to call");
    assert_eq!(res, 7);
}

fn do_swap_contract(container: &Container) -> anyhow::Result<()> {
    let instance = SwapContractInstance::new((3, 100), 257).unwrap();
    let invocation = Invocation {
        now_ts: 1000,
        contract: Contract {
            start_ts: 0,
            end_ts: 0,
            oracle_data_ids: vec!["price/BTC/kraken".to_string()],
            instance,
        },
        oracle_data: vec![10000, 10100],
    };

    let result: SimpleResult = container.typed_call_returning_simple_result("swap_driver", invocation)?;

    assert_eq!(result.outcome, 128 + 42);
    Ok(())
}

fn do_test_stack_overflow(container: &Container) {
    let bad = container.get_func("bad")
        .expect("failed to find `bad` function export")
        .get0::<u32>().expect("mismatch");

    bad().expect_err("should have errored");
}
